1.  我们先用Vivado建立好工程，生成比特流文件，然后在工程目录下找到一个.hdf文件，这个文件就包含了petalinux使用的文件。
2.  把linux_base.sdk目录复制到linux主机中，打开终端，进入工作目录，设置petalinux环境变量，运行命令source  /opt/pkg/petalinux/settings.sh，然后哦设置vivado环境变量，运行命令source  /opt/Xilinx/Vivado/2017.4/settings64.sh，然后创建一个petalinux工程，运行命令petalinux-create  --type project  --template zynq  --name  ax-peta，然后进入petalinux工作目录，运行命令petalinux-config --get-hw-description ../linux_base.sdk配置petalinux工程的硬件信息，然后我们可以对petalinux工程进行配置，本实验基本都是默认配置。
3.  运行命令petalinux-config  -c  kernel配置内核，本实验暂时不需要什么配置，退出即可。
4.  运行命令petalinux-config-c rootfs来配置根文件系统，本实验保持默认配置。
5.  运行命令petalinux-build来配置编译uboot、内核、根文件系统、设备树等。
6.  运行命令petalinux-package --boot --fsbl ./images/linux/zynq_fsbl.elf  --fpga  --u-boot  --force生成BOOT文件。
7.  将工程目录中的BOOT.BIN和image.ub复制到sd卡中，然后插到开发板上，开发板设置为sd卡启动，打开串口终端，启动开发板，使用root登录，默认密码root，插上网线后，使用ifconfig命令可以看到网络状态。